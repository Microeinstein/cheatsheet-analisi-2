#!/bin/bash

for f in *.tex; do
	echo "$f"
	make "$(basename "$f" .tex)".svg
done
make mv
make clean
